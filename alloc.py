#!/usr/bin/python2.7


import argparse 
import time
from collections import defaultdict

def get_highest_register(code_list,iterator): 
    
    highest_register = 0

    for x in range(0,iterator):
        if code_list[x] == []:
            pass
        
        elif code_list[x][0] == 'loadI':
            
            if int(code_list[x][3][1:]) > highest_register:
                highest_register = int(code_list[x][3][1:])
        
        elif code_list[x][0] == 'load' or  code_list[x][0] == 'store':
            
            if int(code_list[x][3][1:]) > highest_register:
                highest_register = int(code_list[x][3][1:])
           
            if int(code_list[x][1][1:]) > highest_register:
                highest_register = int(code_list[x][1][1:])
        
        elif code_list[x][0] == 'add'  or code_list[x][0] == 'mult'  or code_list[x][0] == 'sub' or code_list[x][0] == 'lshift' or code_list[x][0] == 'rshift':
            
            if int(code_list[x][1][1:-1]) > highest_register:
                highest_register = int(code_list[x][1][1:-1])

            if int(code_list[x][2][1:]) > highest_register:
                highest_register = int(code_list[x][2][1:])

            if int(code_list[x][4][1:]) > highest_register:
                highest_register = int(code_list[x][4][1:])

    return highest_register


def main():
    
    start_time = time.time()
    parser = argparse.ArgumentParser(description='CS 415 Project 1 ')
    parser.add_argument('k',help='\'k\' number of registers wanted')
    parser.add_argument('allocator',help='Type of allocator(b,s,t,o)\n b for bottom-down\n s for simple top-down\n t for max live top-down\n o for optimized top down')
    parser.add_argument('filename',help='file name of the ILOC code')
 
    args = vars(parser.parse_args())

    num_of_registers = args['k']
    
    if int(num_of_registers) < 2:
        print "ERROR: Register number must be less then 5 as per project description"
        quit()
    
    allocate_method = args['allocator']
    filename = args['filename']

    file = open(filename, 'r')
    
    code_list_dict = defaultdict(list)
    active_registers = defaultdict(list)
    iterator = 0;

    for line in file:
        if line[0] == '/' and line[1] == '/':
            pass
            #code_list[line] = 0
        else:
            code_list = line.split()
            if code_list == []:
                continue
            code_list_dict[iterator] = code_list
            iterator = iterator + 1
            #code_list[line] = 1
    
    max_line = iterator

    #for x in range(0,iterator):
    #    print str(x) + ' : '+ str(code_list_dict[x])
    
    highest_register = get_highest_register(code_list_dict,iterator) 
    
    register_occurences = []

    #start of x for loop
    for x in range(0,highest_register+1):
        occurence_count = 0
        for j in range(0,iterator):
            
            if code_list_dict[j] == [] or code_list_dict[j][0] == 'output':
                pass
            
            elif code_list_dict[j][0] == 'loadI':
                
                if int(code_list_dict[j][3][1:]) == x:
                    occurence_count = occurence_count + 1
            
            elif code_list_dict[j][0] == 'load' or  code_list_dict[j][0] == 'store':
                
                if int(code_list_dict[j][1][1:]) == x:
                    occurence_count = occurence_count + 1
                
                if int(code_list_dict[j][3][1:]) == x:
                    occurence_count = occurence_count + 1
            
            elif code_list_dict[j][0] == 'add'  or code_list_dict[j][0] == 'mult'  or code_list_dict[j][0] == 'sub' or code_list_dict[j][0] == 'lshift' or code_list_dict[j][0] == 'rshift':
                
                if int(code_list_dict[j][1][1:-1]) == x:
                    occurence_count = occurence_count + 1
 
                if int(code_list_dict[j][2][1:]) == x:
                    occurence_count = occurence_count + 1
 
                if int(code_list_dict[j][4][1:]) == x:
                    occurence_count = occurence_count + 1
 
            else:
                print 'error' + code_list_dict[j][0]

        register_occurences.append(occurence_count)
        
        #start of y for loop
        for y in range(0,iterator):                
             
            if code_list_dict[y] == [] or code_list_dict[y][0] == 'output':
                pass
            
            elif code_list_dict[y][0] == 'loadI':
                
                if int(code_list_dict[y][3][1:]) == x:
                    active_registers[x] = [y,0]
                    break
            
            elif code_list_dict[y][0] == 'load' or  code_list_dict[y][0] == 'store':
                
                if int(code_list_dict[y][1][1:]) == x:
                    active_registers[x] = [y,0]
                    break

                if int(code_list_dict[y][3][1:]) == x:
                    active_registers[x] = [y,0]
                    break

            elif code_list_dict[y][0] == 'add'  or code_list_dict[y][0] == 'mult'  or code_list_dict[y][0] == 'sub' or code_list_dict[y][0] == 'lshift' or code_list_dict[y][0] == 'rshift':
                
                if int(code_list_dict[y][1][1:-1]) == x:
                    active_registers[x] = [y,0]
                    break

                if int(code_list_dict[y][2][1:]) == x:
                    active_registers[x] = [y,0]
                    break

                if int(code_list_dict[y][4][1:]) == x:
                    active_registers[x] = [y,0]
                    break
            
            else:
                print 'error' + code_list_dict[y][0]
        #end of y for loop

        #start of z for loop
        for z in range(iterator-1,0, -1):       
            
            if code_list_dict[z] == [] or code_list_dict[z][0] == 'output':
                pass
            
            elif code_list_dict[z][0] == 'loadI':
                
                if int(code_list_dict[z][3][1:]) == x:
                    active_registers[x][1] = z
                    break
            
            elif code_list_dict[z][0] == 'load' or  code_list_dict[z][0] == 'store':
 
                if int(code_list_dict[z][3][1:]) == x:
                    active_registers[x][1] = z 
                    break               

                if int(code_list_dict[z][1][1:]) == x:
                    active_registers[x][1] = z-1
                    break

            elif code_list_dict[z][0] == 'add'  or code_list_dict[z][0] == 'mult'  or code_list_dict[z][0] == 'sub' or code_list_dict[z][0] == 'lshift' or code_list_dict[z][0] == 'rshift':
                
                if int(code_list_dict[z][4][1:]) == x:
                    active_registers[x][1] = z
                    break
 
                if int(code_list_dict[z][2][1:]) == x:
                    active_registers[x][1] = z-1
                    break

                if int(code_list_dict[z][1][1:-1]) == x:
                    active_registers[x][1] = z-1
                    break
                           
            else:
                print 'error' + code_list_dict[z][0]                
        #end of z for loop
    #end of x for loop

    #testing printing live ranges

    live_ranges = defaultdict(list)
    
    #debugi
    phys_reg = int(num_of_registers)-2
    
    #print "# of physical registers = k-f : "  + str(phys_reg)

    #testing - print the number of occurences
    #for x in range(0,highest_register+1):
    #    print 'r' + str(x) + ' :  '+ str(register_occurences[x])
    
    register_list = ["r" + str(x) for x in range(0,highest_register+1)]
    phys_registers = sorted(zip(register_occurences,register_list), reverse=True)[:phys_reg] 
    
    reverse_list = []

    #print phys_registers

    #for x in range(0,highest_register+1):
    #    print code_list_dict[x]
   
        
    
    #for x in range(0,highest_register+1):
    #    print "r" + str(x) + " -> " + str(virtual_to_phys['r' + str(x)])    

    #for occo,reggo in phys_registers:
        #print 'r' + str(reggo) + ' :  '+ str(occo)
    
    
    allocate_list = defaultdict(list)
    
    counter = 0
    
    allocate_list[0] = code_list_dict[0]
    if allocate_method == 's':
        for x in phys_registers:
            reverse_list.append(reversed(x))

        #print reverse_list

        #reverse_dict = dict(reverse_list)
       
        virtual_to_phys = dict(reverse_list)

        #print virtual_to_phys

        memory = 0
        regia = 3


        
        for x in range(0, highest_register+1):
            if register_occurences[x] == 0:
                virtual_to_phys[register_list[x]] = 0
       
        #print virtual_to_phys   
        
        for x in register_list:
            if x in virtual_to_phys:
                if int(virtual_to_phys[x]) != 0:
                    virtual_to_phys[x] =  'r' + str(regia)
                    regia = regia+1

            else:
                virtual_to_phys[x] = memory
                memory = memory-4
        
        
        
        for j in range(1,max_line):
            counter = counter + 1
            #print counter
            #print j

            if code_list_dict[j] == [] or code_list_dict[j][0] == 'output':    
                allocate_list[counter] = code_list_dict[j]
                 
            elif code_list_dict[j][0] == 'loadI': 
                if code_list_dict[j][3] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][3]],int):
                        allocate_list[counter] = list(code_list_dict[j])
                        allocate_list[counter][3] = 'r1'
                        
                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r1' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][3]]] 

                        #print 'mem' + str(virtual_to_phys[code_list_dict[j][3]])
                    else:
                        allocate_list[counter] = list(code_list_dict[j])
                        allocate_list[counter][3] = virtual_to_phys[code_list_dict[j][3]]  
                else:
                    print 'something broke'

         
            elif code_list_dict[j][0] == 'load' or  code_list_dict[j][0] == 'store':
                passo = 1
                
                if code_list_dict[j][1] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][1]],int):
                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][1]] , '=>' , 'r1']                        
                         
                        counter = counter+1
                        passo = 1
                    else:
                        passo = 0
                else:
                    print 'something broke'

                if code_list_dict[j][3] in virtual_to_phys: 
                    if isinstance(virtual_to_phys[code_list_dict[j][3]],int):

                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][3]] , '=>' , 'r2']                         
                        
                        counter = counter+1
                        
                        allocate_list[counter] = list(code_list_dict[j])
                        
                        if passo == 0:
                            allocate_list[counter][1] =  virtual_to_phys[code_list_dict[j][1]]

                        else:
                            allocate_list[counter][1] = 'r1'
                       
                        allocate_list[counter][3] = 'r2'

                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r2' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][3]]]
                    
                    else:
                        allocate_list[counter] = list(code_list_dict[j])
                        if passo == 0:
                            allocate_list[counter][1] =  virtual_to_phys[code_list_dict[j][1]]

                        else:
                            allocate_list[counter][1] = 'r1'
                        

                        allocate_list[counter][3] = virtual_to_phys[code_list_dict[j][3]]

                else:
                    print 'something broke'
            elif code_list_dict[j][0] == 'add'  or code_list_dict[j][0] == 'mult'  or code_list_dict[j][0] == 'sub' or code_list_dict[j][0] == 'lshift' or code_list_dict[j][0] == 'rshift':
                pasio = 1
                pasia = 1 
                if code_list_dict[j][1][:-1] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][1][:-1]],int):
                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][1][:-1]] , '=>' , 'r1']         
                        counter = counter +1
                        pasio = 1
                    else:
                        pasio = 0
                
                else:
                    print 'borkea 2'

                if code_list_dict[j][2] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][2]],int):
                         
                        allocate_list[counter] = ['loadAI', 'r0, ' , virtual_to_phys[code_list_dict[j][2]] , '=>' , 'r2']                       
                        counter = counter+1
                        pasia = 1
                    else:
                        pasia = 0
                        #allocate_list[counter+2][2] = virtual_to_phys[code_list_dict[j][2]] 
                else:
                    print 'something broke'

                if code_list_dict[j][4] in virtual_to_phys:  
                    allocate_list[counter] = list(code_list_dict[j]) 
                    if pasio == 1:
                        allocate_list[counter][1] = 'r1,'
                    else:
                        allocate_list[counter][1] = virtual_to_phys[code_list_dict[j][1][:-1]] + ','
                    if pasia == 1:
                        allocate_list[counter][2] = 'r2' 
                    else:
                        allocate_list[counter][2] = virtual_to_phys[code_list_dict[j][2]] 
                    
                    if isinstance(virtual_to_phys[code_list_dict[j][4]],int):
                        allocate_list[counter][4] = 'r2' 
                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r2' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][4]]] 
                         
                    else:
                        #counter = counter+
                        allocate_list[counter][4] = virtual_to_phys[code_list_dict[j][4]] 
 
     
            else:
                print 'error' + code_list_dict[j][0]
    
    elif allocate_method == 't': 
        
        phys_registers = sorted(zip(register_occurences,register_list), reverse=True)

        for x in phys_registers:
            reverse_list.append(reversed(x))

        #print reverse_list

        #reverse_dict = dict(reverse_list)
       
        virtual_to_phys = dict(reverse_list)

        #print virtual_to_phys
        max_lives = []
        memory = 0
        
         
        for x in range(0,highest_register+1):
            if active_registers[x] != []:
                for z in range(active_registers[x][0], active_registers[x][1]+1):
                    temp_register = 'r' + str(x)
                    live_ranges[z].append(temp_register)
        
        #for x in live_ranges:
        #    print live_ranges[x]

        max_liv_regs = {}

        regs_to_spill = []
        for x in range(0,max_line):
            #print sorted(live_ranges[x])
            if len(live_ranges[x]) > phys_reg:
                
                for y in live_ranges[x]:
                    #if y == 'ignore':
                    #    continue
                    max_liv_regs[y] = virtual_to_phys[y] 
                
                sortedl = sorted(max_liv_regs.items(), key=lambda x: x[1], reverse = True) 
                # sortedl = sorted(max_liv_regs, reverse=True)
                max_reg_val = 0
                largest_reg = 'r'

                for i, j in sortedl:
                    #print i
                    #print int(i[1:])
                    k = register_occurences[int(i[1:])]
                    if k > max_reg_val:
                        max_reg_val = k
                        largest_reg = i
                
                #print largest_reg
                regs_to_spill.append(largest_reg)
                #tempo = x

                for p in range(0, max_line):
                    if largest_reg in live_ranges[p]:
                        live_ranges[p].remove(largest_reg)
                        #= [x if x != largest_reg else 'ignore' for x in live_ranges[p]]
                #for i, j in sortedl:
                    #print str(i) + str(k)
                #    pass
                
                max_liv_regs = {}

        virtual_to_phys = {}    

        for x in register_list:
            if x in regs_to_spill:
                memory = memory-4
                virtual_to_phys[x] = memory
            if int(register_occurences[int(x[1:])]) == 0:
                virtual_to_phys[x] = 0

        
        start_val = phys_reg
        temp_la = range(0,start_val+1)

        for x in range(0,start_val+1):
            temp_la[x] = 0

        for x in range(1,max_line):
            start_val = phys_reg
            if x == 1:
                for y in live_ranges[x]:
                    virtual_to_phys[y] = 'r' + str(start_val)
                    temp_la[start_val - phys_reg] = y
                    start_val = start_val + 1
            else:
                for y in temp_la:
                    if y not in live_ranges[x]:
                        temp_la[temp_la.index(y)] = 0
                
                for y in live_ranges[x]:
                    if y in virtual_to_phys:
                        continue
                    start_val = temp_la.index(0) + phys_reg
                    virtual_to_phys[y] = 'r' + str(start_val)
                    temp_la[start_val - phys_reg] = y
        
        #for x in range(1,highest_register+1):
        #    print 'r' + str(x) + ' : ' + str(virtual_to_phys['r' + str(x)])
        
        counter = 0 
        for j in range(1,max_line):
            counter = counter + 1

            if code_list_dict[j] == [] or code_list_dict[j][0] == 'output':    
                allocate_list[counter] = code_list_dict[j]
                
            elif code_list_dict[j][0] == 'loadI': 
                if code_list_dict[j][3] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][3]],int):
                        allocate_list[counter] = list(code_list_dict[j])
                        allocate_list[counter][3] = 'r1'
                        
                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r1' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][3]]] 

                        #print 'mem' + str(virtual_to_phys[code_list_dict[j][3]])
                    else:
                        allocate_list[counter] = list(code_list_dict[j])
                        allocate_list[counter][3] = virtual_to_phys[code_list_dict[j][3]]  
                else:
                    print 'something broke'

         
            elif code_list_dict[j][0] == 'load' or  code_list_dict[j][0] == 'store':
                if code_list_dict[j][1] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][1]],int):
                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][1]] , '=>' , 'r1']                        
                         
                        counter = counter+1
                        passo = 1
                    else:
                        passo = 0
                else:
                    print 'something broke'

                if code_list_dict[j][3] in virtual_to_phys:

                    
                    if isinstance(virtual_to_phys[code_list_dict[j][3]],int):

                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][3]] , '=>' , 'r2']                         
                        
                        counter = counter+1
                        
                        allocate_list[counter] = list(code_list_dict[j])
                        
                        if passo == 0:
                            allocate_list[counter][1] =  virtual_to_phys[code_list_dict[j][1]]

                        else:
                            allocate_list[counter][1] = 'r1'
                       
                        allocate_list[counter][3] = 'r2'

                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r2' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][3]]]
                    
                    else: 
                        allocate_list[counter] = list(code_list_dict[j])
                       
                        if passo == 0:
                            allocate_list[counter][1] =  virtual_to_phys[code_list_dict[j][1]]

                        else:
                            allocate_list[counter][1] = 'r1'
                        
                        allocate_list[counter][3] = virtual_to_phys[code_list_dict[j][3]]

                else:
                    print 'something broke'
            elif code_list_dict[j][0] == 'add'  or code_list_dict[j][0] == 'mult'  or code_list_dict[j][0] == 'sub' or code_list_dict[j][0] == 'lshift' or code_list_dict[j][0] == 'rshift':
                if code_list_dict[j][1][:-1] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][1][:-1]],int):
                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][1][:-1]] , '=>' , 'r1']         
                        counter = counter +1
                        pasio = 1
                    else:
                        pasio = 0
                
                else:
                    print 'borkea 2'

                if code_list_dict[j][2] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][2]],int):
                         
                        allocate_list[counter] = ['loadAI', 'r0, ' , virtual_to_phys[code_list_dict[j][2]] , '=>' , 'r2']                       
                        counter = counter+1
                        pasia = 1
                    else:
                        pasia = 0
                        #allocate_list[counter+2][2] = virtual_to_phys[code_list_dict[j][2]] 
                else:
                    print 'something broke'

                if code_list_dict[j][4] in virtual_to_phys:  
                    allocate_list[counter] = list(code_list_dict[j]) 
                    if pasio == 1:
                        allocate_list[counter][1] = 'r1,'
                    else:
                        allocate_list[counter][1] = virtual_to_phys[code_list_dict[j][1][:-1]] + ','
                    if pasia == 1:
                        allocate_list[counter][2] = 'r2' 
                    else:
                        allocate_list[counter][2] = virtual_to_phys[code_list_dict[j][2]] 
                    
                    if isinstance(virtual_to_phys[code_list_dict[j][4]],int):
                        allocate_list[counter][4] = 'r2' 
                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r2' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][4]]] 
                         
                    else:
                        #counter = counter+
                        allocate_list[counter][4] = virtual_to_phys[code_list_dict[j][4]] 
 
     
            else:
                print 'error' + code_list_dict[j][0]
    
    elif allocate_method == 'b': 
        
        phys_registers = sorted(zip(register_occurences,register_list), reverse=True)

        for x in phys_registers:
            reverse_list.append(reversed(x))

        #print reverse_list

        #reverse_dict = dict(reverse_list)
       
        virtual_to_phys = dict(reverse_list)

        #print virtual_to_phys
        max_lives = []
        memory = 0
        
         
        for x in range(0,highest_register+1):
            if active_registers[x] != []:
                for z in range(active_registers[x][0], active_registers[x][1]+1):
                    temp_register = 'r' + str(x)
                    live_ranges[z].append(temp_register)
        
        #for x in live_ranges:
        #    print live_ranges[x]


        virtual_to_phys = {}    

        for x in register_list:
            #if x in regs_to_spill:
            #    memory = memory-4
            #    virtual_to_phys[x] = memory
            if int(register_occurences[int(x[1:])]) == 0:
                virtual_to_phys[x] = 0

        memory = 0
        start_val = phys_reg
        temp_la = range(0,start_val+1)

        for x in range(0,start_val+1):
            temp_la[x] = 0

        for x in range(1,max_line):
            start_val = phys_reg
            if x == 1:
                while len(live_ranges[x]) > phys_reg:
                    farthest_use = 0
                    reg_to_spill = 'r'
                    for y in live_ranges[x]:
                        if active_registers[int(y[1:])][1] > farthest_use:
                            farthest_use = active_registers[int(y[1:])][1]
                            reg_to_spill = y

                    for k in range(x,max_line):
                            if reg_to_spill in live_ranges[k]:
                                live_ranges[k].remove(reg_to_spill)
                    
                    memory = memory-4    
                    virtual_to_phys[reg_to_spill] = memory
                           
                for y in live_ranges[x]:        
                    virtual_to_phys[y] = 'r' + str(start_val)
                    temp_la[start_val - phys_reg] = y
                    start_val = start_val + 1
            else:
                for y in temp_la:
                    if y not in live_ranges[x]:
                        temp_la[temp_la.index(y)] = 0
                
                while len(live_ranges[x]) > phys_reg: 
                    farthest_use = 0
                    reg_to_spill = 'r'
                    for y in live_ranges[x]:
                        if active_registers[int(y[1:])][1] > farthest_use:
                            farthest_use = active_registers[int(y[1:])][1]
                            reg_to_spill = y

                    for k in range(x,max_line):
                            if reg_to_spill in live_ranges[k]:
                                live_ranges[k].remove(reg_to_spill)
                    
                    memory = memory-4    
                    virtual_to_phys[reg_to_spill] = memory
                 
                for y in live_ranges[x]:
                    if y in virtual_to_phys:
                        continue
                    start_val = temp_la.index(0) + phys_reg
                    virtual_to_phys[y] = 'r' + str(start_val)
                    temp_la[start_val - phys_reg] = y
        
         
        counter = 0 
        for j in range(1,max_line):
            counter = counter + 1

            if code_list_dict[j] == [] or code_list_dict[j][0] == 'output':    
                allocate_list[counter] = code_list_dict[j]
                
            elif code_list_dict[j][0] == 'loadI': 
                if code_list_dict[j][3] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][3]],int):
                        allocate_list[counter] = list(code_list_dict[j])
                        allocate_list[counter][3] = 'r1'
                        
                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r1' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][3]]] 

                        #print 'mem' + str(virtual_to_phys[code_list_dict[j][3]])
                    else:
                        allocate_list[counter] = list(code_list_dict[j])
                        allocate_list[counter][3] = virtual_to_phys[code_list_dict[j][3]]  
                else:
                    print 'something broke'

         
            elif code_list_dict[j][0] == 'load' or  code_list_dict[j][0] == 'store':
                if code_list_dict[j][1] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][1]],int):
                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][1]] , '=>' , 'r1']                        
                         
                        counter = counter+1
                        passo = 1
                    else:
                        passo = 0
                else:
                    print 'something broke'

                if code_list_dict[j][3] in virtual_to_phys:

                    
                    if isinstance(virtual_to_phys[code_list_dict[j][3]],int):

                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][3]] , '=>' , 'r2']                         
                        
                        counter = counter+1
                        
                        allocate_list[counter] = list(code_list_dict[j])
                        
                        if passo == 0:
                            allocate_list[counter][1] =  virtual_to_phys[code_list_dict[j][1]]

                        else:
                            allocate_list[counter][1] = 'r1'
                       
                        allocate_list[counter][3] = 'r2'

                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r2' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][3]]]
                    
                    else: 
                        allocate_list[counter] = list(code_list_dict[j])
                       
                        if passo == 0:
                            allocate_list[counter][1] =  virtual_to_phys[code_list_dict[j][1]]

                        else:
                            allocate_list[counter][1] = 'r1'
                        
                        allocate_list[counter][3] = virtual_to_phys[code_list_dict[j][3]]

                else:
                    print 'something broke'
            elif code_list_dict[j][0] == 'add'  or code_list_dict[j][0] == 'mult'  or code_list_dict[j][0] == 'sub' or code_list_dict[j][0] == 'lshift' or code_list_dict[j][0] == 'rshift':
                if code_list_dict[j][1][:-1] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][1][:-1]],int):
                        allocate_list[counter] = ['loadAI', 'r0, ', virtual_to_phys[code_list_dict[j][1][:-1]] , '=>' , 'r1']         
                        counter = counter +1
                        pasio = 1
                    else:
                        pasio = 0
                
                else:
                    print 'borkea 2'

                if code_list_dict[j][2] in virtual_to_phys:
                    if isinstance(virtual_to_phys[code_list_dict[j][2]],int):
                         
                        allocate_list[counter] = ['loadAI', 'r0, ' , virtual_to_phys[code_list_dict[j][2]] , '=>' , 'r2']                       
                        counter = counter+1
                        pasia = 1
                    else:
                        pasia = 0
                        #allocate_list[counter+2][2] = virtual_to_phys[code_list_dict[j][2]] 
                else:
                    print 'something broke'

                if code_list_dict[j][4] in virtual_to_phys:  
                    allocate_list[counter] = list(code_list_dict[j]) 
                    if pasio == 1:
                        allocate_list[counter][1] = 'r1,'
                    else:
                        allocate_list[counter][1] = virtual_to_phys[code_list_dict[j][1][:-1]] + ','
                    if pasia == 1:
                        allocate_list[counter][2] = 'r2' 
                    else:
                        allocate_list[counter][2] = virtual_to_phys[code_list_dict[j][2]] 
                    
                    if isinstance(virtual_to_phys[code_list_dict[j][4]],int):
                        allocate_list[counter][4] = 'r2' 
                        counter = counter+1 
                        allocate_list[counter] = ['storeAI', 'r2' , '=>' , 'r0, ', virtual_to_phys[code_list_dict[j][4]]] 
                         
                    else:
                        #counter = counter+
                        allocate_list[counter][4] = virtual_to_phys[code_list_dict[j][4]] 
 
     
            else:
                print 'error' + code_list_dict[j][0]
    
    
    for x in range(0, counter+1):
        if  allocate_list[x] == []:
            print ''
        
        elif  allocate_list[x][0] == 'loadI':
            print '\t' + str(allocate_list[x][0]) + '\t' + str(allocate_list[x][1]) +'\t' + str(allocate_list[x][2]) + ' ' + str(allocate_list[x][3]) 
        
        elif allocate_list[x][0] == 'load' or  allocate_list[x][0] == 'store':
            print '\t' + str(allocate_list[x][0]) + '\t' + str(allocate_list[x][1]) +'\t' + str(allocate_list[x][2]) + ' ' + str(allocate_list[x][3])  
 
        
        elif allocate_list[x][0] == 'add'  or allocate_list[x][0] == 'mult'  or allocate_list[x][0] == 'sub' or allocate_list[x][0] == 'lshift' or allocate_list[x][0] == 'rshift':
            print '\t' + str(allocate_list[x][0]) + '\t' + str(allocate_list[x][1]) +' ' + str(allocate_list[x][2]) + '\t' + str(allocate_list[x][3]) + ' ' + str(allocate_list[x][4])

        elif allocate_list[x][0] == 'output': 
            print '\t' + str(allocate_list[x][0]) + ' ' + str(allocate_list[x][1]) 
        
        elif allocate_list[x][0] == 'loadAI':
            print '\t' + str(allocate_list[x][0]) + '\t' + str(allocate_list[x][1]) + '\t' + str(allocate_list[x][2]) + '\t' + str(allocate_list[x][3]) + ' ' + str(allocate_list[x][4])

        elif allocate_list[x][0] == 'storeAI':
            print '\t' + str(allocate_list[x][0]) + '\t' + str(allocate_list[x][1]) + ' ' + str(allocate_list[x][2]) + '\t' + str(allocate_list[x][3]) + ' ' + str(allocate_list[x][4])

        else:
            print 'error: unknown opcode'
    
    elapsed_time = time.time() - start_time
    print '//execution time is : ' + str(elapsed_time)
    
    #max_live_reg = 0

        #print str(x) + ' : '+ str(code_list_dict[x]) + ' lives : ' + str(live_ranges[x]) + 'live number : ' + str(len(live_ranges[x]))
        
    #print '\n\n\n'
    #print max_live_reg
    
    #testing printing string
    #code_str = ''
    #for code_line, val in code_list.iteritems():
    #    if val == 0: 
    #        pass
    #   else:
    #        code_str = code_str + code_line

   # print code_str

if __name__ == "__main__":
    main()    
